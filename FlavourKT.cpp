
#include "FlavourKT.h"

using namespace fastjet;

#define DIJ_EPS (1e-6)

//----------------------------------------------------------------------

class FlavourKTBJ
{
  public:
    static const int NONFLAV = FlavourKTPlugin::NONFLAV;

    FlavourKTBJ()
      : pt2(), rap(), phi(), lhid(NONFLAV), beam()
    {}

    void init(const PseudoJet& jet) {
      if (jet.px() == 0. and jet.py() == 0.) { // beam representative
        pt2 = 0.;
        phi = 0.;
        rap = fastjet::MaxRap + abs(jet.pz());
        if (jet.pz() < 0.) {
          rap = -rap;
        }
      } else {
        pt2 = jet.pt2();
        phi = jet.phi();
        rap = jet.rapidity();
      }
      lhid = FlavourKTPlugin::getFlavour(jet);
      beam = FlavourKTPlugin::getBeam(jet);
    }

    double distance(const FlavourKTBJ* other) const {
      // check that flavours are compatible
      const int newlhid = FlavourKTPlugin::combineFlavour(lhid, other->lhid);
      if (newlhid == NONFLAV) {
        return std::numeric_limits<double>::max();
      }
      // cannot fold beams on each other
      if (beam and other->beam) {
        return std::numeric_limits<double>::max();
      }

      double dij = 0.;
      if (pt2 == 0.) {
        dij = other->pt2;
        if (rap != 0.) {
          dij *= 1. - DIJ_EPS*other->rap/rap;
        }
      } else if (other->pt2 == 0.) {
        dij = pt2;
        if (other->rap != 0.) {
          dij *= 1. - DIJ_EPS*rap/other->rap;
        }
      } else {
        double dphi = abs(phi - other->phi);
        if (dphi > M_PI) {
          dphi = 2.*M_PI - dphi;
        }
        const double drap = rap - other->rap;
        dij = std::min(pt2, other->pt2)*(drap*drap + dphi*dphi);
      }
      return dij;
    }

    double beam_distance() const {
      if (pt2 == 0.) {
        return 0.75*std::numeric_limits<double>::max();
      } else {
        return 0.50*std::numeric_limits<double>::max();
      }
    }

  protected:
    static double sqr(const double x) { return x*x; }

    double pt2;
    double rap;
    double phi;
    int lhid;
    bool beam;
};

std::string FlavourKTPlugin::description() const
{
  return std::string("FlavourKTPlugin: if you don't know what it is, don't use it.");
}

void FlavourKTPlugin::run_clustering(ClusterSequence& cs) const
{
  int njets = cs.jets().size();
  NNH<FlavourKTBJ> nnh(cs.jets());

  while (njets > 0) {
    int i, j, k;
    double dij = nnh.dij_min(i, j);  // i,j are return values

    const PseudoJet& jet1 = cs.jets()[i];
    if (not isQCD(getFlavour(jet1))) {
      dij = 0;
    }
    if (j >= 0) {
      const PseudoJet& jet2 = cs.jets()[j];
      if (not isQCD(getFlavour(jet2))) {
        dij = 0;
      }
      cs.plugin_record_ij_recombination(i, j, dij, combine(jet1, jet2), k);
      nnh.merge_jets(i, j, cs.jets()[k], k);
    } else {
      if (dij > 1e300) {
        dij = 0.;
      }
      cs.plugin_record_iB_recombination(i, dij);
      nnh.remove_jet(i);
    }

    njets--;
  }
}

void FlavourKTPlugin::copyFlavour(const fastjet::PseudoJet& src, fastjet::PseudoJet& dst)
{
  dst.set_user_index(src.user_index());
}

void FlavourKTPlugin::addFlavour(PseudoJet& jet, int lhid, bool beam)
{
//   FlavourInfo* finfo = new FlavourInfo(lhid);
//   jet.set_user_info(finfo);
  jet.set_user_index((lhid << 1) | (beam ? 1 : 0));
}

int FlavourKTPlugin::getFlavour(const PseudoJet& jet)
{
  return jet.user_index() >> 1;
}

bool FlavourKTPlugin::getBeam(const PseudoJet& jet)
{
  return jet.user_index() & 1;
}

PseudoJet FlavourKTPlugin::combine(const PseudoJet& jet1, const PseudoJet& jet2) const
{
  PseudoJet newjet;
  // if both are < pt2cut use beam merging, otherwise use vector merging
  if (jet1.pt2() < pt2cut and jet2.pt2() < pt2cut) {
    if (getBeam(jet1)) {
      newjet = PseudoJet(0., 0., jet1.pz() - jet2.E(), jet1.E() - jet2.E());
    } else if (getBeam(jet2)) {
      newjet = PseudoJet(0., 0., jet2.pz() - jet1.E(), jet2.E() - jet1.E());
    } else {
      newjet = jet1 + jet2;
    }
  } else {
    if (getBeam(jet1)) {
      newjet = jet1 - jet2;
    } else if (getBeam(jet2)) {
      newjet = jet2 - jet1;
    } else {
      newjet = jet1 + jet2;
    }
  }
  addFlavour(newjet, combineFlavour(getFlavour(jet1), getFlavour(jet2)),
                     getBeam(jet1) or getBeam(jet2));
  return newjet;
}

int FlavourKTPlugin::combineFlavour(int lh1, int lh2)
{
  bool q1 = isQCD(lh1);
  bool q2 = isQCD(lh2);

  if (lh2 == 21) {
    std::swap(lh1, lh2);
    std::swap(q1, q2);
  }
  if (lh1 == 21) {
    if (q2 or lh2 == 25) {
      return lh2;
    } else {
      return NONFLAV;
    }
  }

  if (q2) {
    std::swap(lh1, lh2);
    std::swap(q1, q2);
  }
  if (q1) { // quark/anti-quark
    if (lh1 == -lh2) {
      return 21;
    } else if (not q2 and lh2 != 25) {
      return lh1;
    } else {
      return NONFLAV;
    }
  }

  return NONFLAV;
}

#if defined(__MAKECINT__)
#pragma link C++ class fastjet::JetDefinition;
#pragma link C++ class FlavourKTPlugin;
#pragma link C++ class FlavourKTBJ;
#endif
