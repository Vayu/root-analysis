#!/usr/bin/env python

import ROOT
import numpy as np
import os
import re
import sys
import math


# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------

# Grid helper
def create_grid(name, edges, params):
    if not params.grids:
        return None
    if params.warmup and os.path.exists(name):
        bakname = name + '.bak'
        print "WARNING: warmup file exists, moving '%s' to '%s'" % (name, bakname)
        os.rename(name, bakname)
    if not os.path.exists(name):
        if not params.warmup:
            print "ERROR: No --warmup option, but grid '%s' is not found" % name
            sys.exit(2)
        obs = ROOT.std.vector('double')()
        for ed in edges:
            obs.push_back(ed)
        grid = ROOT.Grid.capture(ROOT.Grid(name, obs))
    else:
        grid = ROOT.Grid.capture(ROOT.Grid(name))
    if not grid.isWarmup():
        assert not params.warmup
        grid.setFilename(name.replace('.root', '-o.root'))
    return grid

# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------

def add_histograms_all(analysis, params, smear=[0.]):
    # Quadratic slope = lastbinwidth/firstbinwidth
    # ROOT.LinearHistogram(file_name, hist_name, n_bins, obs_min, obs_max)
    # ROOT.QuadraticHistogram(file_name, hist_name, n_bins, obs_min, obs_max, slope)
    # ROOT.SmearedLinearHistogram(file_name, hist_name, n_bins, obs_min, obs_max, smear_fac in [0, 1])
    # ROOT.SmearedQuadraticHistogram(file_name, hist_name, n_bins, obs_min, obs_max, slope, smear_fac in [0, 1])

    # List histogram takes a vector of bin edges
    # bin_edges = ROOT.std.vector('double')()
    # map(bin_edges.push_back, [1, 2, 3, 4, 5, 6])  # 5 bins in [1, 6)
    # ROOT.ListHistogram(file_name, hist_name, bin_edges)

    def get_filename(s):
        name = "l_s%g" % (s,)
        filename = (params.output % name) + '.hist'
        return filename

    def Histogram(s, *args):
        if s == 0.:
            if len(args) == 5:
                print "LinearHistogram ", args
                return ROOT.LinearHistogram(*args)
            if len(args) == 3:
                print "ListHistogram ", args
                bin_edges = ROOT.std.vector('double')()
                map(bin_edges.push_back, args[2])
                return ROOT.ListHistogram(args[0], args[1], bin_edges)
        else:
            if len(args) == 5:
                args += (s,)  # add smearing parameter
                print "SmearedHistogram", args
                return ROOT.SmearedLinearHistogram(*args)
            if len(args) == 3:
                print "(ignoring smearing) ListHistogram ", args
                bin_edges = ROOT.std.vector('double')()
                map(bin_edges.push_back, args[2])
                return ROOT.ListHistogram(args[0], args[1], bin_edges)

    def AddHistogram(hvec, *args):
        for s in smear:
            filename = get_filename(s)
            hvec.push_back(Histogram(s, filename, *args))

    PI = math.pi
    H_pT_bins = [0.,5.,10.,15.,20.,25.,30.,35.,40.,45.,50.,55.,60.,
                  65.,70.,75.,80.,85.,90.,95.,100.,110.,120.,130.,140.,150.,
                  160.,170.,180.,190.,200.]

    H_pT_jj_bins  = [0.,10.,20.,30.,40.,50.,60.,70.,80.,90.,100.,110.,120.,
                     130.,140.,150.,160.,170.,180.,190.,200.,220.,240.,260.,
                     280.,300.,320.,340.,360.,380.,400.]

    Hj_pT_bins  = [0.,5.,10.,15.,20.,25.,30.,35.,40.,45.,50.,55.,60.,
                   65.,70.,75.,80.,85.,90.,95.,100.,110.,120.,130.,140.,150.,
                   160.,170.,180.,190.,200.,220.,240.,260.,280.,300.,
                   320.,340.,360.,380.,400.]

    H_y_bins  = [0.,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,
                 1.4,1.5,1.6,1.7,1.8,1.9,2.,2.1,2.2,2.3,2.4,2.5,2.7,2.9,
                 3.1,3.3,3.5,4.0,4.5,5]

    jet_y_bins  = [0.,0.25,0.5,0.75,1.,1.25,1.5,1.75,2.,2.25,2.5,
                   3.,3.5,4.,4.4]

    deltaphi_jj_bins  = [0.,PI/16.,2.*PI/16.,3.*PI/16.,4.*PI/16.,
                         5.*PI/16.,6.*PI/16.,7.*PI/16.,8.*PI/16.,
                         9.*PI/16.,10.*PI/16.,11.*PI/16.,12.*PI/16.,
                         13.*PI/16.,14.*PI/16.,15.*PI/16.,PI]

    deltay_jj_bins  = [0.,0.5,1.,1.5,2.,2.5,3.,3.5,4.,4.5,5.,5.5,6.,
                       6.5,7.,7.5,8.,8.5,9.,9.5,10.]

    dijet_mass_bins  = [0.,50.,100.,150.,200.,250.,300.,350.,400.,450.,
                        500.,550.,600.,650.,700.,750.,800.,850.,900.,
                        950.,1000.]
    deltay_yy_bins  = [0,0.3,0.6,0.9,1.2,1.5,2.0,2.55]
    dR_y_j_bins  = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.2,1.4,1.6,1.8,
                    2.0,2.4,2.8,3.2,3.6,4.0,4.5,5.0,5.5,6.0,7.0,8.0,10.0]

    tau_jet_bins  = [0.,4.,8.,12.,16.,20.,25.,30.,40.,60.,85.]
    HT_bins  = [0.,30.,40.,50.,60.,70.,90.,110.,130.,150.,200.,
                250.,300.,400.,500.,600.,800.,1000.]

    pTt_bins  = [0.,10.,20.,30.,40.,60.,80.,150.,500.]
    deltaphi_Hjj_bins  = [0.0,1.0,2.0,2.3,2.6,2.8,2.9,3.,3.05,3.1,PI]

    deltay_H_jj_bins  = [0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8,2.0,2.2,2.4,2.6,
                         2.8,3.0,3.5,4.0,5.0,6.0,8.0]

    delta_phi2_bins  = [-PI,-3.0,-2.8,-2.6,-2.4,-2.2,-2.0,-1.8,-1.6,-1.4,-1.2,
                        -1.,-0.8,-0.6,-0.4,-0.2,0.,0.2,0.4,0.6,0.8,1.,1.2,1.4,
                        1.6,1.8,2.0,2.2,2.4,2.6,2.8,3.0,PI]

    H_dijet_mass_bins  = [0.,100.,150.,200.,250.,300.,350.,400.,450.,500.,550.,
                          600.,650.,700.,750.,800.,850.,900.,950.,1000.,1100.,
                          1200.,1300.,1400.,1500.,1700.,2000.]

    jetveto_bins = np.logspace(0,3,num=301)

    histdefs = [
        ["higgs_pt"                      , ("higgs_pt_20_200", 20, 0, 200)],
        ["higgs_pt"                      , ("higgs_pt_25_500", 25, 0, 500)],

        ["higgs_pt"                      , ("H_pT_incl", H_pT_bins)],
        ["higgs_pt_excl"                 , ("H_pT_excl", H_pT_bins)],

        ["higgs_pt_j_incl"               , ("H_j_pT_incl", H_pT_bins)],
        ["higgs_pt_jj_incl"              , ("H_jj_pT_incl", H_pT_jj_bins)],
        ["higgs_pt_jjj_incl"             , ("H_jjj_pT_incl", H_pT_jj_bins)],

        ["higgs_pt_j_excl"               , ("H_j_pT_excl", H_pT_bins)],
        ["higgs_pt_jj_excl"              , ("H_jj_pT_excl", H_pT_jj_bins)],
        ["higgs_pt_jjj_excl"             , ("H_jjj_pT_excl", H_pT_jj_bins)],

        ["higgs_pt_jj_VBF"               , ("H_jj_pT_VBF", H_pT_jj_bins)],
        ["higgs_pt_jj_VBF2"              , ("H_jj_pT_VBF2", H_pT_jj_bins)],

        ["higgs_absy"                    , ("H_y", H_y_bins)],

        ["higgs_jet_pt"                  , ("Hj_pT_incl", Hj_pT_bins)],
        ["higgs_jet_pt_excl"             , ("Hj_pT_excl", Hj_pT_bins)],

        ["higgs_dijet_pt"                , ("Hjj_pT_incl", Hj_pT_bins)],
        ["higgs_dijet_pt_excl"           , ("Hjj_pT_excl", Hj_pT_bins)],
        ["higgs_dijet_mass"              , ("H_dijet_mass", H_dijet_mass_bins)],
        ["higgs_dijet_dy"                , ("deltay_H_jj", deltay_H_jj_bins)],
        ["higgs_dijet_dphi"              , ("deltaphi_Hjj_incl", deltaphi_Hjj_bins)],
        ["higgs_dijet_dphi_excl"         , ("deltaphi_Hjj_excl", deltaphi_Hjj_bins)],
        ["higgs_dijet_dphi_VBF"          , ("deltaphi_Hjj_VBF", deltaphi_Hjj_bins)],
        ["higgs_dijet_dphi_VBF2"         , ("deltaphi_Hjj_VBF2", deltaphi_Hjj_bins)],
        ["higgs_dijet_phi2"              , ("deltaphi2", delta_phi2_bins)],
        ["higgs_dijet_phi2_VBF"          , ("deltaphi2_VBF", delta_phi2_bins)],
        ["higgs_dijet_phi2_VBF2"         , ("deltaphi2_VBF2", delta_phi2_bins)],

        ["jet_pt_n"                      , ("jet_pT_%d_20_200", 20, 0, 200)],
        ["jet_pt_n"                      , ("jet_pT_%d_25_500", 25, 0, 500)],

        ["jet_pt_n"                      , ("jet%d_pT_incl", H_pT_bins)],
        ["jet_pt_n_exclusive"            , ("jet%d_pT_excl", H_pT_bins)],
        ["jet_y_n"                       , ("jet%d_y", jet_y_bins)],

        ["higgs_jet_tau_i"               , ("tau_jet%d", tau_jet_bins)],
        ["higgs_jet_tau_max"             , ("tau_jet_max", tau_jet_bins)],
        ["higgs_jet_tau_sum"             , ("sum_tau_jet", tau_jet_bins)],

        ["dijet_dphi"                    , ("deltaphi_jj_incl", deltaphi_jj_bins)],
        ["dijet_dphi_excl"               , ("deltaphi_jj_excl", deltaphi_jj_bins)],
        ["dijet_dphi_VBF"                , ("deltaphi_jj_VBF", deltaphi_jj_bins)],
        ["dijet_dphi_VBF2"               , ("deltaphi_jj_VBF2", deltaphi_jj_bins)],
        ["dijet_mass"                    , ("dijet_mass", dijet_mass_bins)],
        ["dijet_dy"                      , ("deltay_jj", deltay_jj_bins)],

        ["dijet_dy"                      , ("jjpT_dy", 18, 0, 9)],
        ["dijet_dy_2j_excl"              , ("jjpT_dy_2j_excl", 18, 0, 9)],
        ["dijet_dy_3j_excl"              , ("jjpT_dy_3j_excl", 18, 0, 9)],
        ["dijetRAP_dy"                   , ("jjdy_dy", 18, 0, 9)],
        ["dijetRAP_dy_2j_excl"           , ("jjdy_dy_2j_excl", 18, 0, 9)],
        ["dijetRAP_dy_3j_excl"           , ("jjdy_dy_3j_excl", 18, 0, 9)],

        ["jets_ht"                       , ("HT_jets", HT_bins)],
        ["higgs_Et_ht"                   , ("HT_all", HT_bins)],

        ["eff_jet1veto"                  , ("xs_jet_veto_j0", jetveto_bins)],
        ["eff_jet2veto_jet1_30"          , ("xs_jet_veto_j1_30", jetveto_bins)],
        ["eff_jet2veto_jet1_50"          , ("xs_jet_veto_j1_50", jetveto_bins)],
        ["eff_jet2veto_jet1_100"         , ("xs_jet_veto_j1_100", jetveto_bins)],
        ["eff_jet2veto_jet1_200"         , ("xs_jet_veto_j1_200", jetveto_bins)],
        ["eff_jet2veto_jet1_500"         , ("xs_jet_veto_j1_500", jetveto_bins)],
        ["eff_jet2veto_jet1_1000"        , ("xs_jet_veto_j1_1000", jetveto_bins)],
        ["eff_jet2veto_higgs_50"         , ("xs_jet_veto_h_50", jetveto_bins)],
        ["eff_jet2veto_higgs_100"        , ("xs_jet_veto_h_100", jetveto_bins)],
        ["eff_jet2veto_higgs_200"        , ("xs_jet_veto_h_200", jetveto_bins)],
        ["eff_jet2veto_higgs_500"        , ("xs_jet_veto_h_500", jetveto_bins)],

    ]

    for hname, hparam in sorted(histdefs):
        histparam = hparam[1:]
        assert len(histparam) >= 1
        if '%d%d' in hparam[0]:
            for j in range(1, params.njet+1):
                for i in range(j):
                    histname = hparam[0] % (i+1, j+1)
                    n = (j-1)*j/2 + i
                    AddHistogram(getattr(analysis, hname)[n], histname, *histparam)
        elif '%d' in hparam[0]:
            for i in range(params.njet+1):
                histname = hparam[0] % (i+1)
                AddHistogram(getattr(analysis, hname)[i], histname, *histparam)
        else:
            histname = hparam[0]
            AddHistogram(getattr(analysis, hname), histname, *histparam)

    return

# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------

def add_grids_all(analysis, params):
    # total xs grid
    if True:
        obs = [-0.5, 0.5]  # only one bin to save memory
        filename = (params.output % 'totxs') + '.root'  # has to end with '.root'
        analysis.g_jet_inclusive = create_grid(filename, obs, params)

    # inclusive jets grid
    if False:
        obs = (lambda n: np.linspace(-0.5, n+0.5, n+2))(params.njet+1)
        filename = (params.output % 'incl') + '.root'  # has to end with '.root'
        analysis.g_jet_inclusive = create_grid(filename, obs, params)

    if False:
        obs = np.linspace(0, 500, 15+1)
        filename = (params.output % 'phmass') + '.root'  # has to end with '.root'
        analysis.g_photon_mass = create_grid(filename, obs, params)

    return

# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------

# main function which is called from hammer.py
def initialize(params, selector):
    print "Using higgs analysis %s" % __file__

    jet_R = 0.4
    jetrpat = r'-R(\d+\.?\d*)-'
    m = re.match(r".*?%s.*?" % jetrpat, params.output)
    if m:
        jet_R = float(m.group(1))

    analysis = ROOT.HiggsJetsAnalysis.create()
    analysis.runname = params.runname
    analysis.setJetNumber(params.njet)
    analysis.setAntiKt(jet_R)
    analysis.jet_ptmin = 30
    analysis.jet_etamax = 4.4
    analysis.min_dijet_m = 400
    analysis.min_dijet_y = 2.8
    analysis.minHjtau = 8
    selector.opt_extra_scale = 125
    selector.opt_extra_alphas = 0
    selector.opt_extra_factor = 1
    if params.rescaler in ['mult_higgs', 'fixed_higgs', 'hthat_higgs']:
        selector.opt_extra_alphas = 0
    if params.rescaler == 'multiplicative':
        selector.opt_extra_factor = selector.opt_rescale_factor

    if 'good' in params.output:
        if params.composite == 'split':
            selector.opt_extra_factor = selector.opt_rescale_factor
        else:
            selector.opt_extra_alphas = 0

    print "Extra alphas=%d factor=%f scale=%f" % (selector.opt_extra_alphas,
        selector.opt_extra_factor, selector.opt_extra_scale)

    # Extract smear value from the output pattern, e.g. -smear0.3- or -smear0,0.1,0.3-
    smearpat = r'-smear(\d+\.?\d*|[\d.,]+)-'
    m = re.match(r".*?%s.*?" % smearpat, params.output)
    if m:
        params.output = re.sub(smearpat, '-', params.output)
        val = m.group(1)
        try:
            smear = [float(val)]
        except ValueError:
            smear = eval(val)
    else:
        smear = [0.]

    if not params.grids:
        # Add analysis histograms (see the function above)
        add_histograms_all(analysis, params, smear=smear)
    else:
        # Setting up grids
        ROOT.Grid.nloops = 1                     # number of loops, 0 - LO, 1 - NLO
        ROOT.Grid.pdf_function = "ntuplejets"    # 'ntuplephjets' for photons, 'ntuplejets' for jets
        ROOT.Grid.aparam = 5.
        ROOT.Grid.born_alphaspower = selector.opt_born_alphaspower
        # set the limits on x1, x2 and Q2
        fac = selector.opt_rescale_factor
        if 'vbf' in params.output:
            ROOT.Grid.def_opts = ROOT.GridOpts(100, (90*fac)**2, (4000*fac)**2, 5,
                                               100, 0.0044, 1., 5)
        else:
            ROOT.Grid.def_opts = ROOT.GridOpts(100, (90*fac)**2, (4000*fac)**2, 5,
                                               100, 0.00054, 1., 5)
        add_grids_all(analysis, params)

    # assign to selector
    selector.analysis = analysis


if __name__ == '__main__':
    print "Analysis module can't be run alone"
    print "pass --analysis=%s to hammer.py" % __file__
