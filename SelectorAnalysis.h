
#ifndef SELECTOR_ANALYSIS_H
#define SELECTOR_ANALYSIS_H

#include "SelectorCommon.h"
#include "SelectorHistograms.h"

class SelectorCommon;

class Analysis
{
  public:
    typedef std::vector<fastjet::PseudoJet> PseudoJetVector;
    typedef std::vector<std::vector<HistogramBase*> > HistogramVector;
    static Analysis* create() { return new Analysis(); }

    Analysis();
    virtual ~Analysis();

    void set_input(PseudoJetVector newinput);
    virtual bool check_cuts(const SelectorCommon* event);
    virtual void analysis_bin(const SelectorCommon* event);
    virtual void analysis_finalize(const SelectorCommon* event, bool last=true);

    PseudoJetVector input;
    PseudoJetVector jets, jetsRAP;

    void setJetNumber(const unsigned n);
    void setAntiKt(double R);
    void setKt(double R);

    fastjet::JetDefinition jet_definition;
    unsigned jet_number;
    bool jet_rapidity_tag;
    bool jet_mass_tag;

    double jet_ptmin;
    double jet_etamax;
    double jet_ymax;

    double call_count;
    double event_count;
    double event_binned;

    LinearHistogram* jet_exclusive;
    LinearHistogram* jet_inclusive;
    std::vector<HistogramBase*> scale_wgt;
    std::vector<HistogramBase*> scale_nowgt;
    std::vector<HistogramBase*> jet_ht;
    std::vector<std::vector<HistogramBase*> > jet_pt_n;
    std::vector<std::vector<HistogramBase*> > jet_pt_n_exclusive;
    std::vector<std::vector<HistogramBase*> > jet_eta_n;
    std::vector<std::vector<HistogramBase*> > jet_y_n;

    Grid* g_jet_inclusive;
    std::vector<Grid*> g_jet_pt_n;
    std::vector<Grid*> g_jet_eta_n;

    TString runname;

    virtual void reset();

  protected:
    std::set<TString> outputfiles;

    template <typename T>
    void clear_var(T*& var);

    void append_output_filename(const TString& name);

    void clear_histvec(std::vector<HistogramBase*>& histvec);
    void clear_histvec(std::vector<std::vector<HistogramBase*> >& histvecvec);
    void output_histvec(const std::vector<HistogramBase*>& histvec,
                        const TString& filename, std::ofstream& stream,
                        bool dryrun);

    void bin_histvec(const std::vector<HistogramBase*>& histvec,
                     int nextevt, double x, double w);
    void bin_histvec(const std::vector<HistogramBase*>& histvec,
                     int nextevt, double x, double y, double w);

    // fill efficiency / vetoed cross section histograms
    void bin_eff_histvec(const std::vector<HistogramBase*>& histvec,
                     int nextevt, double x, double w);



    virtual void output_histograms(const TString& filename, std::ofstream& stream,
                                   bool dryrun);
    virtual void output_grids();
    virtual void clear();
    virtual void finalize_stat(std::ostream& stream, const SelectorCommon* event);

    void fill_grid(Grid* grid, int nextevt, double x, double w, const SelectorCommon* event);

    bool photonIsolation(const SelectorCommon* event, double photon_R,
                         double photon_n, double photon_eps) const;
};

class JetAnalysis : public Analysis
{
  public:
    static JetAnalysis* create() { return new JetAnalysis(); }

    JetAnalysis();

    virtual bool check_cuts(const SelectorCommon* event);
    virtual void analysis_bin(const SelectorCommon* event);

    double jet_pt1min;
    double jet_pt2min;
    double jet_pt3min;

    double jet_ht2min;
    double jet_deltaRmin;

    std::vector<HistogramBase*> jet_pt12ave;

  protected:
    virtual void output_histograms(const TString& filename, std::ofstream& stream,
                                   bool dryrun);
    virtual void clear();
};

class Jet3Analysis : public JetAnalysis
{
  public:
    typedef JetAnalysis BaseClass;

    static Jet3Analysis* create() { return new Jet3Analysis(); }

    Jet3Analysis();

    virtual bool check_cuts(const SelectorCommon* event);
    virtual void analysis_bin(const SelectorCommon* event);

    double jet_eta1max, jet_eta2max, jet_eta2min;
    double jet_jet_DR23min, jet_jet_DR23max, jet_jet_M12min;

    std::vector<HistogramBase*> jet_jet_eta23;
    std::vector<HistogramBase*> jet_jet_phi23;
    std::vector<HistogramBase*> jet_jet_beta23;

  protected:
    virtual void output_histograms(const TString& filename, std::ofstream& stream,
                                   bool dryrun);
    virtual void clear();
};

// another MPI senstive observable from Gavin Salam
class JetPairAnalysis : public JetAnalysis
{
  public:
    typedef JetAnalysis BaseClass;

    static JetPairAnalysis* create() { return new JetPairAnalysis(); }

    JetPairAnalysis();

    bool check_pair(fastjet::PseudoJet& jetA, fastjet::PseudoJet& jetB, double ptmin);
    virtual bool check_cuts(const SelectorCommon* event);
    virtual void analysis_bin(const SelectorCommon* event);

    double jets_01_ptmin, jets_23_ptmin;
    double jetpair_ratiomin, jetpair_dphimin, jetpair_dymax;
    double jets_deltaRmin;

    std::vector<HistogramBase*> jet_01_dphi;
    std::vector<HistogramBase*> jet_23_dphi;
    std::vector<HistogramBase*> jet_pairs_dphi;

  protected:
    virtual void output_histograms(const TString& filename, std::ofstream& stream,
                                   bool dryrun);
    virtual void clear();
};

class FourJetMPIAnalysis : public JetAnalysis
{
  public:
    typedef JetAnalysis BaseClass;

    static FourJetMPIAnalysis* create() { return new FourJetMPIAnalysis(); }

    FourJetMPIAnalysis();
    int mpivars_d12_bins;
    double mpivars_d12_bin_low;
    double mpivars_d12_bin_high;

    virtual bool check_cuts(const SelectorCommon* event);
    virtual void analysis_bin(const SelectorCommon* event);

    std::vector<HistogramBase*> jets_d12_d34;

  protected:
    virtual void output_histograms(const TString& filename, std::ofstream& stream,
                                   bool dryrun);
    virtual void clear();
};

class JetMAnalysis : public JetAnalysis
{
  public:
    typedef JetAnalysis BaseClass;

    static JetMAnalysis* create() { return new JetMAnalysis(); }

    JetMAnalysis();

    virtual bool check_cuts(const SelectorCommon* event);
    virtual void analysis_bin(const SelectorCommon* event);

    double ystar_min;
    double ystar_max;

    std::vector<HistogramBase*> jet_mass_jjj;

    Grid* g_jet_mass_jjj;

  protected:
    virtual void output_histograms(const TString& filename, std::ofstream& stream,
                                   bool dryrun);
    virtual void output_grids();
    virtual void clear();
};

class PhotonJetAnalysis : public Analysis
{
  public:
    static PhotonJetAnalysis* create() { return new PhotonJetAnalysis(); }

    PhotonJetAnalysis();

    virtual bool check_cuts(const SelectorCommon* event);
    virtual void analysis_bin(const SelectorCommon* event);

    double jet_pt1min;  // extra cut on leading jet-pt
    double photon_R;
    double photon_n;
    double photon_eps;
    double photon_ptmin;
    double photon_etamax;
    double photon_jet_Rsep;

    std::vector<HistogramBase*> photon_pt;
    std::vector<HistogramBase*> photon_eta;
    std::vector<HistogramBase*> photon_jet_R11;
    std::vector<HistogramBase*> jet_jet_phi12;

    Grid* g_photon_pt;
    Grid* g_photon_eta;
    Grid* g_photon_jet_R11;
    Grid* g_jet_jet_phi12;

    virtual void reset();

  protected:
    virtual void output_histograms(const TString& filename, std::ofstream& stream,
                                   bool dryrun);
    virtual void output_grids();
    virtual void clear();
};

class VJetAnalysis : public Analysis
{
  public:
    static VJetAnalysis* create() { return new VJetAnalysis(); }

    VJetAnalysis();

    virtual bool check_cuts(const SelectorCommon* event);
    virtual void analysis_bin(const SelectorCommon* event);

    double lepton_ptmin;
    double lepton_etamax;
    double lepton_etagap_min;
    double lepton_etagap_max;
    double etmiss_min;
    double vboson_mass_min;
    double vboson_mass_max;
    double lepton_jet_Rsep;
    double lepton_lepton_Rsep;
    double vboson_onshell_mass;

    fastjet::PseudoJet vboson;  // store vector boson momentum

    std::vector<HistogramBase*> vboson_pt;
    std::vector<HistogramBase*> vboson_eta;

    Grid* g_vboson_pt;
    Grid* g_vboson_eta;

    virtual void reset();

  protected:
    virtual void output_histograms(const TString& filename, std::ofstream& stream,
                                   bool dryrun);
    virtual void output_grids();
    virtual void clear();
};

class DiPhotonAnalysis : public Analysis
{
  public:
    static DiPhotonAnalysis* create() { return new DiPhotonAnalysis(); }

    DiPhotonAnalysis();

    virtual bool check_cuts(const SelectorCommon* event);
    virtual void analysis_bin(const SelectorCommon* event);

    double jet_pt1min;  // extra cut on leading jet-pt

    double photon_R;
    double photon_n;
    double photon_eps;
    double photon_pt1min;
    double photon_pt2min;
    double photon_etamax;
    double photon_etagap_min;
    double photon_etagap_max;

    double photon_photon_Rsep;
    double photon_jet_Rsep;
    double photon_photon_mass_min;
    double photon_photon_mass_max;

    std::vector<HistogramBase*> photon_mass;
    std::vector<HistogramBase*> photon_pt;
    std::vector<HistogramBase*> photon_eta;
    std::vector<HistogramBase*> photon_jet_R11;
    std::vector<HistogramBase*> jet_jet_phi12;
    std::vector<HistogramBase*> jet_jet_mass;
    std::vector<HistogramBase*> jet_jet_eta12;
    std::vector<HistogramBase*> diphoton_dijet_phi;
    std::vector<HistogramBase*> diphoton_dijet_ystar;

    Grid* g_photon_mass;
    Grid* g_photon_pt;
    Grid* g_photon_eta;
    Grid* g_photon_jet_R11;
    Grid* g_jet_jet_phi12;

    virtual void reset();

  protected:
    virtual void output_histograms(const TString& filename, std::ofstream& stream,
                                   bool dryrun);
    virtual void output_grids();
    virtual void clear();
};

class DiPhotonAnalysisBH : public DiPhotonAnalysis
{
  public:
    static DiPhotonAnalysisBH* create() { return new DiPhotonAnalysisBH(); }

    virtual bool check_cuts(const SelectorCommon* event);
};

class HiggsJetsAnalysis : public Analysis
{
  public:
    static HiggsJetsAnalysis* create() { return new HiggsJetsAnalysis(); }

    HiggsJetsAnalysis();

    virtual bool check_cuts(const SelectorCommon* event);
    virtual void analysis_bin(const SelectorCommon* event);

    bool vbfcuts;
    double min_dijet_m;
    double min_dijet_y;
    double minHjtau;

    LinearHistogram* jet_30_inclusive;
    LinearHistogram* jet_30_exclusive;

    LinearHistogram* jet_50_inclusive;
    LinearHistogram* jet_50_exclusive;

    LinearHistogram* jet_VBF_inclusive;
    LinearHistogram* jet_VBF_exclusive;

    LinearHistogram* jet_VBF2_inclusive;
    LinearHistogram* jet_VBF2_exclusive;

    LinearHistogram* jet_jhj_inclusive;
    LinearHistogram* jet_jhj_exclusive;

    std::vector<std::vector<HistogramBase*> > jet_jet_mass_ij;
    std::vector<std::vector<HistogramBase*> > jet_jet_pt_ij;
    std::vector<std::vector<HistogramBase*> > jet_jet_pt_ij_excl;
    std::vector<std::vector<HistogramBase*> > jet_jet_dy_ij;
    std::vector<std::vector<HistogramBase*> > jet_jet_dphi_ij;
    std::vector<std::vector<HistogramBase*> > jet_jet_dphi_ij_excl;
    std::vector<std::vector<HistogramBase*> > jet_jet_dR_ij;

    std::vector<HistogramBase*> higgs_pt;
    std::vector<HistogramBase*> higgs_pt_excl;
    std::vector<HistogramBase*> higgs_eta;
    std::vector<HistogramBase*> higgs_y;
    std::vector<HistogramBase*> higgs_absy;
    std::vector<HistogramBase*> higgs_pt_j_incl;
    std::vector<HistogramBase*> higgs_pt_jj_incl;
    std::vector<HistogramBase*> higgs_pt_jjj_incl;
    std::vector<HistogramBase*> higgs_pt_j_excl;
    std::vector<HistogramBase*> higgs_pt_jj_excl;
    std::vector<HistogramBase*> higgs_pt_jjj_excl;
    std::vector<HistogramBase*> higgs_pt_jj_VBF;
    std::vector<HistogramBase*> higgs_pt_jj_VBF2;

    std::vector<HistogramBase*> dijet_mass;
    std::vector<HistogramBase*> dijet_dy;
    std::vector<HistogramBase*> dijet_dphi;
    std::vector<HistogramBase*> dijet_dphi_excl;
    std::vector<HistogramBase*> dijet_dphi_VBF;
    std::vector<HistogramBase*> dijet_dphi_VBF2;

    std::vector<HistogramBase*> dijet_dy_2j_excl;
    std::vector<HistogramBase*> dijet_dy_3j_excl;
    std::vector<HistogramBase*> dijetRAP_dy;
    std::vector<HistogramBase*> dijetRAP_dy_2j_excl;
    std::vector<HistogramBase*> dijetRAP_dy_3j_excl;

    std::vector<HistogramBase*> jjj_ystar;

    std::vector<HistogramBase*> parton_hthat;
    std::vector<HistogramBase*> higgs_mt_hthat;
    std::vector<HistogramBase*> higgs_pt_hthat;
    std::vector<HistogramBase*> higgs_Et_hthat;

    std::vector<HistogramBase*> jets_ht;
    std::vector<HistogramBase*> higgs_mt_ht;
    std::vector<HistogramBase*> higgs_pt_ht;
    std::vector<HistogramBase*> higgs_Et_ht;

    std::vector<HistogramBase*> mT1_hj12;
    std::vector<HistogramBase*> mT1_hjets;

    std::vector<HistogramBase*> mT2_hj12;
    std::vector<HistogramBase*> mT2_hjets;

    std::vector<HistogramBase*> mT2_j12h;

    std::vector<HistogramBase*> higgs_jet_pt;
    std::vector<HistogramBase*> higgs_jet_pt_excl;

    std::vector<HistogramBase*> higgs_dijet_pt;
    std::vector<HistogramBase*> higgs_dijet_pt_excl;
    std::vector<HistogramBase*> higgs_dijet_mass;
    std::vector<HistogramBase*> higgs_dijet_dy;
    std::vector<HistogramBase*> higgs_dijet_dphi;
    std::vector<HistogramBase*> higgs_dijet_dphi_excl;
    std::vector<HistogramBase*> higgs_dijet_dphi_VBF;
    std::vector<HistogramBase*> higgs_dijet_dphi_VBF2;
    std::vector<HistogramBase*> higgs_dijet_phi2;
    std::vector<HistogramBase*> higgs_dijet_phi2_VBF;
    std::vector<HistogramBase*> higgs_dijet_phi2_VBF2;

    std::vector<std::vector<HistogramBase*> > higgs_jet_mass_i;
    std::vector<std::vector<HistogramBase*> > higgs_jet_pt_i;
    std::vector<std::vector<HistogramBase*> > higgs_jet_dphi_i;
    std::vector<std::vector<HistogramBase*> > higgs_jet_dy_i;
    std::vector<std::vector<HistogramBase*> > higgs_jet_dR_i;
    std::vector<std::vector<HistogramBase*> > higgs_jet_tau_i;
    std::vector<HistogramBase*> higgs_jet_tau_max;
    std::vector<HistogramBase*> higgs_jet_tau_sum;

    std::vector<std::vector<HistogramBase*> > higgs_dijet_pt_ij;
    std::vector<std::vector<HistogramBase*> > higgs_dijet_dphi_ij;
    std::vector<std::vector<HistogramBase*> > higgs_dijet_ystar_ij;

    std::vector<HistogramBase*> eff_jet1veto;
    std::vector<HistogramBase*> eff_jet2veto_jet1_30;
    std::vector<HistogramBase*> eff_jet2veto_jet1_50;
    std::vector<HistogramBase*> eff_jet2veto_jet1_100;
    std::vector<HistogramBase*> eff_jet2veto_jet1_200;
    std::vector<HistogramBase*> eff_jet2veto_jet1_500;
    std::vector<HistogramBase*> eff_jet2veto_jet1_1000;
    std::vector<HistogramBase*> eff_jet2veto_higgs_50;
    std::vector<HistogramBase*> eff_jet2veto_higgs_100;
    std::vector<HistogramBase*> eff_jet2veto_higgs_200;
    std::vector<HistogramBase*> eff_jet2veto_higgs_500;

    virtual void reset();

  protected:
    virtual void output_histograms(const TString& filename, std::ofstream& stream,
                                   bool dryrun);
    virtual void clear();
};

class TtHiggsAnalysis : public Analysis
{
  public:
    static TtHiggsAnalysis* create() { return new TtHiggsAnalysis(); }

    TtHiggsAnalysis();

    virtual bool check_cuts(const SelectorCommon* event);
    virtual void analysis_bin(const SelectorCommon* event);

    double top_pt;
    double atop_pt;

    std::vector<std::vector<HistogramBase*> > jet_jet_mass_ij;
    std::vector<std::vector<HistogramBase*> > jet_jet_pt_ij;
    std::vector<std::vector<HistogramBase*> > jet_jet_dy_ij;
    std::vector<std::vector<HistogramBase*> > jet_jet_dphi_ij;
    std::vector<std::vector<HistogramBase*> > jet_jet_dR_ij;

    std::vector<HistogramBase*> higgs_pt;
    std::vector<HistogramBase*> higgs_eta;
    std::vector<HistogramBase*> higgs_y;
    std::vector<HistogramBase*> higgs_mt;

    std::vector<HistogramBase*> t_pt;
    std::vector<HistogramBase*> t_eta;
    std::vector<HistogramBase*> t_y;
    std::vector<HistogramBase*> t_mt;

    std::vector<HistogramBase*> tbar_pt;
    std::vector<HistogramBase*> tbar_eta;
    std::vector<HistogramBase*> tbar_y;
    std::vector<HistogramBase*> tbar_mt;

    std::vector<HistogramBase*> ttbar_pt;
    std::vector<HistogramBase*> ttbar_eta;
    std::vector<HistogramBase*> ttbar_y;
    std::vector<HistogramBase*> ttbar_mt;
    std::vector<HistogramBase*> ttbar_mass;

    std::vector<HistogramBase*> higgsttbar_pt;

    std::vector<HistogramBase*> parton_hthat;
    std::vector<HistogramBase*> mt_hthat;
    std::vector<HistogramBase*> jets_ht;
    std::vector<HistogramBase*> mt_ht;

    /* std::vector<HistogramBase*> mT1_hj12; */
    /* std::vector<HistogramBase*> mT1_hjets; */
    /* std::vector<HistogramBase*> mT2_hj12; */
    /* std::vector<HistogramBase*> mT2_hjets; */
    /* std::vector<HistogramBase*> mT2_j12h; */

    std::vector<std::vector<HistogramBase*> > higgs_jet_mass_i;
    std::vector<std::vector<HistogramBase*> > higgs_jet_pt_i;
    std::vector<std::vector<HistogramBase*> > higgs_jet_dphi_i;
    std::vector<std::vector<HistogramBase*> > higgs_jet_dy_i;
    std::vector<std::vector<HistogramBase*> > higgs_jet_dR_i;

    std::vector<std::vector<HistogramBase*> > higgs_dijet_pt_ij;
    std::vector<std::vector<HistogramBase*> > higgs_dijet_dphi_ij;
    std::vector<std::vector<HistogramBase*> > higgs_dijet_ystar_ij;

    virtual void reset();

  protected:
    virtual void output_histograms(const TString& filename, std::ofstream& stream,
                                   bool dryrun);
    virtual void clear();
};

#if defined(__MAKECINT__)
#pragma link C++ class Analysis;
#pragma link C++ class JetAnalysis;
#pragma link C++ class Jet3Analysis;
#pragma link C++ class FourJetMPIAnalysis;
#pragma link C++ class JetPairAnalysis;
#pragma link C++ class JetMAnalysis;
#pragma link C++ class VJetAnalysis;
#pragma link C++ class PhotonJetAnalysis;
#pragma link C++ class DiPhotonAnalysis;
#pragma link C++ class DiPhotonAnalysisBH;
#pragma link C++ class HiggsJetsAnalysis;
#pragma link C++ class TtHiggsAnalysis;
#pragma link C++ class std::vector<HistogramBase*>;
#pragma link C++ class std::vector<std::vector<HistogramBase*> >;
#pragma link C++ class std::vector<Grid*>;
#endif

#endif // SELECTOR_ANALYSIS_H
