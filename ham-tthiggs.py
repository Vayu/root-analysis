#!/usr/bin/env python

import ROOT
import numpy as np
import os
import re
import sys
import math


# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------

# Grid helper
def create_grid(name, edges, params):
    if not params.grids:
        return None
    if params.warmup and os.path.exists(name):
        bakname = name + '.bak'
        print "WARNING: warmup file exists, moving '%s' to '%s'" % (name, bakname)
        os.rename(name, bakname)
    if not os.path.exists(name):
        if not params.warmup:
            print "ERROR: No --warmup option, but grid '%s' is not found" % name
            sys.exit(2)
        obs = ROOT.std.vector('double')()
        for ed in edges:
            obs.push_back(ed)
        grid = ROOT.Grid.capture(ROOT.Grid(name, obs))
    else:
        grid = ROOT.Grid.capture(ROOT.Grid(name))
    if not grid.isWarmup():
        assert not params.warmup
        grid.setFilename(name.replace('.root', '-o.root'))
    return grid

# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------

def add_histograms_all(analysis, params, smear=[0.]):
    # Quadratic slope = lastbinwidth/firstbinwidth
    # ROOT.LinearHistogram(file_name, hist_name, n_bins, obs_min, obs_max)
    # ROOT.QuadraticHistogram(file_name, hist_name, n_bins, obs_min, obs_max, slope)
    # ROOT.SmearedLinearHistogram(file_name, hist_name, n_bins, obs_min, obs_max, smear_fac in [0, 1])
    # ROOT.SmearedQuadraticHistogram(file_name, hist_name, n_bins, obs_min, obs_max, slope, smear_fac in [0, 1])

    # List histogram takes a vector of bin edges
    # bin_edges = ROOT.std.vector('double')()
    # map(bin_edges.push_back, [1, 2, 3, 4, 5, 6])  # 5 bins in [1, 6)
    # ROOT.ListHistogram(file_name, hist_name, bin_edges)

    def get_filename(s):
        name = "l_s%g" % (s,)
        filename = (params.output % name) + '.hist'
        return filename

    def Histogram(s, *args):
        if s == 0.:
            print "LinearHistogram ", args
            return ROOT.LinearHistogram(*args)
        else:
            args += (s,)  # add smearing parameter
            print "SmearedHistogram", args
            return ROOT.SmearedLinearHistogram(*args)

    def AddHistogram(hvec, *args):
        for s in smear:
            filename = get_filename(s)
            hvec.push_back(Histogram(s, filename, *args))

    histdefs = [
        ["higgs_pt"             , ("higgs_pt_200", 200, 0, 500)],
        ["higgs_pt"             , ("higgs_pt_100", 100, 0, 500)],
        ["higgs_pt"             , ("higgs_pt_50", 50, 0, 500)],
        ["higgs_pt"             , ("higgs_pt_25", 25, 0, 500)],
        ["higgs_eta"            , ("higgs_eta_20", 20, -4.4, 4.4)],
        ["higgs_eta"            , ("higgs_eta_40", 40, -4.4, 4.4)],
        ["higgs_y"              , ("higgs_y_20", 20, -4.4, 4.4)],
        ["higgs_y"              , ("higgs_y_40", 40, -4.4, 4.4)],
        ["jet_pt_n"             , ("jet_pT_%d_200", 200, 0, 500)],
        ["jet_pt_n"             , ("jet_pT_%d_100", 100, 0, 500)],
        ["jet_pt_n"             , ("jet_pT_%d_50", 50, 0, 500)],
        ["jet_eta_n"            , ("jet_eta_%d_20", 20, -4.4, 4.4)],
        ["jet_eta_n"            , ("jet_eta_%d_40", 40, -4.4, 4.4)],
        ["jet_y_n"              , ("jet_y_%d_20", 20, -4.4, 4.4)],
        ["jet_y_n"              , ("jet_y_%d_40", 40, -4.4, 4.4)],
        ["jet_jet_mass_ij"      , ("jet_jet_mass_%d%d_100", 100, 0, 1000)],
        ["jet_jet_pt_ij"        , ("jet_jet_pt_%d%d_100", 100, 0, 500)],
        ["jet_jet_dy_ij"        , ("jet_jet_dy_%d%d_20", 20, 0, 10)],
        ["jet_jet_dphi_ij"      , ("jet_jet_dphi_%d%d_20", 20, 0, math.pi+1e-10)],
        ["jet_jet_dR_ij"        , ("jet_jet_dR_%d%d_50", 50, 0, 5)],

        ["t_pt"                 , ("t_pt_200", 200, 0, 500)],
        ["t_pt"                 , ("t_pt_100", 100, 0, 500)],
        ["t_pt"                 , ("t_pt_50", 50, 0, 500)],
        ["t_pt"                 , ("t_pt_50", 25, 0, 500)],
        ["t_eta"                , ("t_eta_20", 20, -4.4, 4.4)],
        ["t_eta"                , ("t_eta_40", 40, -4.4, 4.4)],
        ["t_y"                  , ("t_y_20", 20, -4.4, 4.4)],
        ["t_y"                  , ("t_y_40", 40, -4.4, 4.4)],
        ["t_mt"                 , ("t_mt_200", 200, 0, 600)],
        ["t_mt"                 , ("t_mt_100", 100, 0, 600)],
        ["t_mt"                 , ("t_mt_50", 50, 0, 600)],

        ["tbar_pt"              , ("tbar_pt_200", 200, 0, 500)],
        ["tbar_pt"              , ("tbar_pt_100", 100, 0, 500)],
        ["tbar_pt"              , ("tbar_pt_50", 50, 0, 500)],
        ["tbar_eta"             , ("tbar_eta_ck", 40, -5.0, 5.0)],
        ["tbar_eta"             , ("tbar_eta_20", 20, -4.4, 4.4)],
        ["tbar_eta"             , ("tbar_eta_40", 40, -4.4, 4.4)],
        ["tbar_y"               , ("tbar_y_20", 20, -4.4, 4.4)],
        ["tbar_y"               , ("tbar_y_40", 40, -4.4, 4.4)],
        ["tbar_mt"              , ("tbar_mt_200", 200, 0, 600)],
        ["tbar_mt"              , ("tbar_mt_100", 100, 0, 600)],
        ["tbar_mt"              , ("tbar_mt_50", 50, 0, 600)],

        ["ttbar_pt"             , ("ttbar_pt_200", 200, 0, 1000)],
        ["ttbar_pt"             , ("ttbar_pt_100", 100, 0, 1000)],
        ["ttbar_pt"             , ("ttbar_pt_50", 50, 0, 1000)],
        ["ttbar_mass"           , ("ttbar_mass_200", 200, 200, 1000)],
        ["ttbar_mass"           , ("ttbar_mass_100", 100, 200, 1000)],
        ["ttbar_mass"           , ("ttbar_mass_50",  50, 200, 1000)],

        ["higgsttbar_pt"        , ("higgsttbar_pt_200", 200, 0, 1000)],
        ["higgsttbar_pt"        , ("higgsttbar_pt_100", 100, 0, 1000)],
        ["higgsttbar_pt"        , ("higgsttbar_pt_50", 50, 0, 1000)],

        ["higgs_jet_mass_i"     , ("higgs_jet_mass_%d_60", 100, 0, 500)],
        ["higgs_jet_pt_i"       , ("higgs_jet_pt_%d_100", 100, 0, 500)],
        ["higgs_jet_dphi_i"     , ("higgs_jet_dphi_%d_20", 20, 0, math.pi+1e-10)],
        ["higgs_jet_dy_i"       , ("higgs_jet_dy_%d_20", 20, 0, 10)],
        ["higgs_jet_dR_i"       , ("higgs_jet_dR_%d_20", 20, 0, 5)],

        ["higgs_dijet_pt_ij"    , ("higgs_dijet_pt_%d%d_60", 60, 0, 300)],
        ["higgs_dijet_dphi_ij"  , ("higgs_dijet_dphi_%d%d_20", 20, 0, math.pi+1e-10)],
        ["higgs_dijet_ystar_ij" , ("higgs_dijet_ystar_%d%d_20", 20, 0, 10)],
        ["parton_hthat"         , ("parton_hthat_100", 100, 30, 1030)],
        ["mt_hthat"             , ("mt_hthat_100", 100, 125, 125+1000)],
        ["jets_ht"              , ("jets_ht_100", 100, 30, 1030)],
        ["mt_ht"                , ("mt_ht_100", 100, 125, 125+1000)]
    ]

    for hname, hparam in sorted(histdefs):
        histparam = hparam[1:]
        assert len(histparam) >= 3
        if '%d%d' in hparam[0]:
            for j in range(1, params.njet+1):
                for i in range(j):
                    histname = hparam[0] % (i+1, j+1)
                    n = (j-1)*j/2 + i
                    AddHistogram(getattr(analysis, hname)[n], histname, *histparam)
        elif '%d' in hparam[0]:
            for i in range(params.njet+1):
                histname = hparam[0] % (i+1)
                AddHistogram(getattr(analysis, hname)[i], histname, *histparam)
        else:
            histname = hparam[0]
            AddHistogram(getattr(analysis, hname), histname, *histparam)

    return

# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------

def add_grids_all(analysis, params):
    # total xs grid
    if True:
        obs = [-0.5, 0.5]  # only one bin to save memory
        filename = (params.output % 'totxs') + '.root'  # has to end with '.root'
        analysis.g_jet_inclusive = create_grid(filename, obs, params)

    # inclusive jets grid
    if False:
        obs = (lambda n: np.linspace(-0.5, n+0.5, n+2))(params.njet+1)
        filename = (params.output % 'incl') + '.root'  # has to end with '.root'
        analysis.g_jet_inclusive = create_grid(filename, obs, params)

    if False:
        obs = np.linspace(0, 500, 15+1)
        filename = (params.output % 'phmass') + '.root'  # has to end with '.root'
        analysis.g_photon_mass = create_grid(filename, obs, params)

    return

# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
# init some fields immediately after selector is created
selector_vars = {
    'opt_histogram_step'   : 200e6,
}

# main function which is called from hammer.py
def initialize(params, selector):
    print "Using tth analysis %s" % __file__

    jet_R = 0.5
    jetrpat = r'-R(\d+\.?\d*)-'
    m = re.match(r".*?%s.*?" % jetrpat, params.output)
    if m:
        jet_R = float(m.group(1))
    print "Jet radius=", jet_R

    analysis = ROOT.TtHiggsAnalysis.create()
    analysis.runname = params.runname
    analysis.setJetNumber(params.njet)
    if re.match(r".*?%s.*?" % '-kt-', params.output):
        analysis.setKt(jet_R)
    else:
        analysis.setAntiKt(jet_R)

    analysis.jet_ptmin = 10
    if params.output.find('ptmin20') >= 0:
        analysis.jet_ptmin = 20
        print "--> ptmin cut set to 20 GeV"        
    elif params.output.find('ptmin15') >= 0:
        analysis.jet_ptmin = 15
        print "--> ptmin cut set to 15 GeV"        
    else:
        print "--> ptmin cut set to 10 GeV"

    analysis.jet_etamax = 4.0
    selector.opt_extra_scale = 471.4

    print "Extra alphas=%d factor=%f scale=%f" % (selector.opt_extra_alphas,
        selector.opt_extra_factor, selector.opt_extra_scale)

    # Extract smear value from the output pattern, e.g. -smear0.3- or -smear0,0.1,0.3-
    smearpat = r'-smear(\d+\.?\d*|[\d.,]+)-'
    m = re.match(r".*?%s.*?" % smearpat, params.output)
    if m:
        params.output = re.sub(smearpat, '-', params.output)
        val = m.group(1)
        try:
            smear = [float(val)]
        except ValueError:
            smear = eval(val)
    else:
        smear = [0.]

    if not params.grids:
        # Add analysis histograms (see the function above)
        add_histograms_all(analysis, params, smear=smear)
    else:
        # Setting up grids
        ROOT.Grid.nloops = 1                     # number of loops, 0 - LO, 1 - NLO
        ROOT.Grid.pdf_function = "ntuplejets"    # 'ntuplephjets' for photons, 'ntuplejets' for jets
        ROOT.Grid.aparam = 5.
        ROOT.Grid.born_alphaspower = selector.opt_born_alphaspower
        selector.opt_born_alphaspower = -1
        # set the limits on x1, x2 and Q2
        fac = selector.opt_rescale_factor
        if '-E13-' in params.output:
            ROOT.Grid.def_opts = ROOT.GridOpts(100, (100*fac)**2, (6500*fac)**2, 5,
                                               100, 0.00025, 1., 5)
        else:
            ROOT.Grid.def_opts = ROOT.GridOpts(100, (100*fac)**2, (4000*fac)**2, 5,
                                               100, 0.00080, 1., 5)
        add_grids_all(analysis, params)

    # assign to selector
    selector.analysis = analysis


if __name__ == '__main__':
    print "Analysis module can't be run alone"
    print "pass --analysis=%s to hammer.py" % __file__
