
all:
	python hammer.py --noapplgrid -a ham-jets --warmup -o 'dummy-%s.tmp' -n 2 -s 1. -f 'CT14nlo' -t 'CT14nlo' dummy.root
	rm -f dummy-*.tmp.hist dummy-*.tmp.root

lhapdf5:
	python hammer.py --noapplgrid -a ham-jets --warmup -o 'dummy-%s.tmp' -n 2 -s 1. -f 'CT10nlo.LHgrid' -t 'CT10nlo.LHgrid' dummy.root
	rm -f dummy-*.tmp.hist dummy-*.tmp.root


clean:
	rm -f *.d *.o *.so *_ACLiC_*

libroot-analysis.so: root-analysis.cpp SelectorCommon.h
	$(CXX) -shared -fPIC $(CXXFLAGS) -o $@ $^ `python-config --includes` `python-config --libs`
