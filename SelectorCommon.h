
#ifndef SELECTOR_COMMON_H
#define SELECTOR_COMMON_H

#include <Rtypes.h>

// Stuff
#include <fastjet/ClusterSequence.hh>

// Math
#include <inttypes.h>
#include <cstdlib>
#include <cmath>
using std::abs;
using std::pow;
using std::sqrt;

#include "SherpaAlphaS.h"

#ifndef DISABLE_LOOPSIM
#include <LoopSim.hh>
#endif // DISABLE_LOOPSIM

namespace RootAnalysis {
class NTupleEvent;
}

class SelectorReader;
class Analysis;

class SelectorCommon
{
  public :
    // Declaration of leaf types
    const Int_t*          input_id;
    const Int_t*          input_nparticle;
    const Int_t*          input_ncount;
    const Float_t*        input_px;   //[nparticle]
    const Float_t*        input_py;   //[nparticle]
    const Float_t*        input_pz;   //[nparticle]
    const Float_t*        input_E;    //[nparticle]
    const Double_t*       input_pxd;  //[nparticle]
    const Double_t*       input_pyd;  //[nparticle]
    const Double_t*       input_pzd;  //[nparticle]
    const Double_t*       input_Ed;   //[nparticle]
    const Double_t*       input_alphas;
    const Int_t*          input_kf;   //[nparticle]
    const Double_t*       input_ps_wgt;
    const Double_t*       input_weight;
    const Double_t*       input_weight2;
    const Double_t*       input_me_wgt;
    const Double_t*       input_me_wgt2;
    const Double_t*       input_kfac;
    const Double_t*       input_x1;
    const Double_t*       input_x2;
    const Double_t*       input_x1p;
    const Double_t*       input_x2p;
    const Int_t*          input_id1;
    const Int_t*          input_id2;
    const Int_t*          input_id1p;
    const Int_t*          input_id2p;
    const Double_t*       input_fac_scale;
    const Double_t*       input_ren_scale;
    const Int_t*          input_nuwgt;
    const Double_t*       input_usr_wgts;   //[nuwgt]
    const Char_t*         input_part;
    const Char_t*         input_alphaspower;

    // NTuples type
    const Bool_t*         input_ed_ntuples;

    // Accessor methods
    Int_t           get_event_id() const { return *input_id; }
    Int_t           get_nparticle() const { return *input_nparticle; }
    Int_t get_ncount() const { 
      if(*(input_ed_ntuples)){
	return *input_ncount;
      }
      else {
	return event_trials;
      }
    }
    Bool_t          is_ed_ntuples() const { return *input_ed_ntuples; }
    Double_t        get_px(int i) const {
      if(*(input_ed_ntuples)) {
    	//std::cout<<"px double = "<<input_pxd[i]<<std::endl;
    	return input_pxd[i];
      }
      else {
    	//std::cout<<"px float = "<<input_px[i]<<std::endl;
    	return(double) input_px[i];
      }
    }
    Double_t        get_py(int i) const {
      if(*(input_ed_ntuples)) {
    	return input_pyd[i];
      }
      else {
    	return(double) input_py[i];
      }
    }
    Double_t        get_pz(int i) const {
      if(*(input_ed_ntuples)) {
    	return input_pzd[i];
      }
      else {
    	return(double) input_pz[i];
      }
    }
    Double_t        get_E(int i) const {
      if(*(input_ed_ntuples)) {
    	return input_Ed[i];
      }
      else {
    	return(double) input_E[i];
      }
    }
    Double_t        orig_alphas() const { return *input_alphas; }
    const Int_t*    get_kf() const { return input_kf; }
    Int_t           get_kf(int i) const { return input_kf[i]; }
    Double_t        orig_ps_wgt() const { return *input_ps_wgt; }
    Double_t        orig_weight() const { return *input_weight; }
    Double_t        orig_weight2() const { return *input_weight2; }
    Double_t        orig_me_wgt() const { return *input_me_wgt; }
    Double_t        orig_me_wgt2() const { return *input_me_wgt2; }
    Double_t        get_x1() const { return *input_x1; }
    Double_t        get_x2() const { return *input_x2; }
    Double_t        get_x1p() const { return *input_x1p; }
    Double_t        get_x2p() const { return *input_x2p; }
    Int_t           get_id1() const { return *input_id1; }
    Int_t           get_id2() const { return *input_id2; }
    Int_t           get_id1p() const { return *input_id1p; }
    Int_t           get_id2p() const { return *input_id2p; }
    Double_t        orig_fac_scale() const { return *input_fac_scale; }
    Double_t        orig_ren_scale() const { return *input_ren_scale; }
    Int_t           get_nuwgt() const { return *input_nuwgt; }
    const Double_t* orig_usr_wgts() const { return input_usr_wgts; }
    Double_t        orig_usr_wgts(int i) const { return input_usr_wgts[i]; }
    Int_t           get_alphaspower() const { return Int_t(*input_alphaspower) - opt_extra_alphas; }
    const Char_t*   get_part() const { return input_part; }
    Char_t          get_part(int i) const { return input_part[i]; }

    void Init(const SelectorReader* reader);
    void Init(const RootAnalysis::NTupleEvent* event);
    bool Process();
    void SlaveBegin();
    void SlaveTerminate();

    SelectorCommon();
    ~SelectorCommon();

    // analysis
    Analysis* analysis;
    typedef std::vector<fastjet::PseudoJet> PseudoJetVector;

    fastjet::PseudoJet get_vec(int i) const;
    PseudoJetVector get_fjinput() const;

#ifndef DISABLE_LOOPSIM
    std::vector<LSParticle> get_lsinput() const;
    static PseudoJetVector lsinput2fjinput(const std::vector<LSParticle>& in);
#endif


    enum {MODE_PLAIN=0, MODE_LOOPSIM};
    int analysis_mode;

    void process_single_event(bool do_reweight=true);

    // scale change
    typedef double (SelectorCommon::*RescalerType)(const double scale,
                                                   const PseudoJetVector& partons,
                                                   const PseudoJetVector& jets);
    double rescaler_multiplicative(const double scale,
                                   const PseudoJetVector& partons,
                                   const PseudoJetVector& jets);
    double rescaler_fixed(const double scale,
                          const PseudoJetVector& partons,
                          const PseudoJetVector& jets);

    // pure jets scales
    double rescaler_ht(const double scale,
                       const PseudoJetVector& partons,
                       const PseudoJetVector& jets);
    double rescaler_hthat(const double scale,
                          const PseudoJetVector& partons,
                          const PseudoJetVector& jets);
    double rescaler_sumpt2(const double scale,
                           const PseudoJetVector& partons,
                           const PseudoJetVector& jets);
    double rescaler_sumpt2hat(const double scale,
                              const PseudoJetVector& partons,
                              const PseudoJetVector& jets);
    // one-nonqcd + jets scales
    double rescaler_m1hthat(const double scale,
                            const PseudoJetVector& partons,
                            const PseudoJetVector& jets);
    double rescaler_mt1hthat(const double scale,
                             const PseudoJetVector& partons,
                             const PseudoJetVector& jets);
    double rescaler_mt1ht(const double scale,
                            const PseudoJetVector& partons,
                            const PseudoJetVector& jets);
    double rescaler_m1sumpt2(const double scale,
                           const PseudoJetVector& partons,
                           const PseudoJetVector& jets);

    // two-nonqcd + jets scales
    double rescaler_m2hthat(const double scale,
                            const PseudoJetVector& partons,
                            const PseudoJetVector& jets);
    double rescaler_mt2hthat(const double scale,
                             const PseudoJetVector& partons,
                             const PseudoJetVector& jets);

    double rescaler_maa(const double scale,
                       const PseudoJetVector& input,
                       const PseudoJetVector& jets);
    double rescaler_maaht(const double scale,
                       const PseudoJetVector& input,
                       const PseudoJetVector& jets);
    double rescaler_mwFhthat(const double scale,
                       const PseudoJetVector& input,
                       const PseudoJetVector& jets);
    double rescaler_maa2sumpt2(const double scale,
                           const PseudoJetVector& input,
                           const PseudoJetVector& jets);
    double rescaler_maa2sumpt2hat(const double scale,
                              const PseudoJetVector& input,
                              const PseudoJetVector& jets);
    
    // ttbar+higgs  scales
    double rescaler_2gahthat(const double scale,
			     const PseudoJetVector& partons,
			     const PseudoJetVector& jets);

    double rescaler_2hthat(const double scale,
			   const PseudoJetVector& partons,
			   const PseudoJetVector& jets);


    // effective higgs scales
    void rescaler_higgs_helper(double newscale, double extra_scale, int extra_power);
    double rescaler_mult_higgs(const double scale,
                          const PseudoJetVector& input,
                          const PseudoJetVector& jets);
    double rescaler_fixed_higgs(const double scale,
                          const PseudoJetVector& input,
                          const PseudoJetVector& jets);
    double rescaler_hthat_higgs(const double scale,
                          const PseudoJetVector& input,
                          const PseudoJetVector& jets);

    // composite rescalers
    double rescaler_split(const double scale,
                          const PseudoJetVector& input,
                          const PseudoJetVector& jets);

    double rescaler_minlo(const double scale,
                          const PseudoJetVector& input,
                          const PseudoJetVector& jets);

    void setrescaler_none() { opt_rescaler = 0; }
    void setrescaler_multiplicative() { opt_rescaler = &SelectorCommon::rescaler_multiplicative; }
    void setrescaler_fixed() { opt_rescaler = &SelectorCommon::rescaler_fixed; }
    void setrescaler_ht() { opt_rescaler = &SelectorCommon::rescaler_ht; }
    void setrescaler_hthat() { opt_rescaler = &SelectorCommon::rescaler_hthat; }
    void setrescaler_2hthat() { opt_rescaler = &SelectorCommon::rescaler_2hthat; }
    void setrescaler_2gahthat() { opt_rescaler = &SelectorCommon::rescaler_2gahthat; }
    void setrescaler_m1hthat() { opt_rescaler = &SelectorCommon::rescaler_m1hthat; }
    void setrescaler_mt1hthat() { opt_rescaler = &SelectorCommon::rescaler_mt1hthat; }
    void setrescaler_m2hthat() { opt_rescaler = &SelectorCommon::rescaler_m2hthat; }
    void setrescaler_mt2hthat() { opt_rescaler = &SelectorCommon::rescaler_mt2hthat; }
    void setrescaler_mt1ht() { opt_rescaler = &SelectorCommon::rescaler_mt1ht; }
    void setrescaler_sumpt2() { opt_rescaler = &SelectorCommon::rescaler_sumpt2; }
    void setrescaler_m1sumpt2() { opt_rescaler = &SelectorCommon::rescaler_m1sumpt2; }
    void setrescaler_sumpt2hat() { opt_rescaler = &SelectorCommon::rescaler_sumpt2hat; }
    void setrescaler_maaht() { opt_rescaler = &SelectorCommon::rescaler_maaht; }
    void setrescaler_maa2sumpt2() { opt_rescaler = &SelectorCommon::rescaler_maa2sumpt2; }
    void setrescaler_maa2sumpt2hat() { opt_rescaler = &SelectorCommon::rescaler_maa2sumpt2hat; }
    void setrescaler_mwFhthat() { opt_rescaler = &SelectorCommon::rescaler_mwFhthat; }
    void setrescaler_mult_higgs() { opt_rescaler = &SelectorCommon::rescaler_mult_higgs; }
    void setrescaler_fixed_higgs() { opt_rescaler = &SelectorCommon::rescaler_fixed_higgs; }
    void setrescaler_hthat_higgs() { opt_rescaler = &SelectorCommon::rescaler_hthat_higgs; }

    void setrescaler_minlo();
    void setrescaler_split();

    int get_max_nparticle();

    // static functions
    static const double Nf;
    static const double CA;
    static const double CF;
    static const double b0;
    static const double b1;

    static int pdg2lha(int pdgnum);
    static double adim(Int_t flav);
    double Deltaf(double Q0sq, double Qsq, int flav);
    double Deltaf1(double Q0sq, double Qsq, int flav);
    double LambdaQCD(double muR=91.188, double aS=-1.) const;

    // member variables
    // reweighting variables
    double opt_rescale_factor;
    int opt_rescale_n;
    RescalerType opt_rescaler;
    int opt_extra_alphas;     // number of extra alphas powers
    double opt_extra_scale;   // original scale for extra alphas powers
    double opt_extra_factor;  // reweighting factor for extra scale
    RescalerType opt_split_core;

    double fac_scalefactor;
    double ren_scalefactor;
    double alphafactor;

    // minlo-specific
    bool opt_minlo_onlyqcd;
    bool opt_minlo_sudakov;
    bool opt_minlo_sudakov_nlo;
    int opt_minlo_minprimary;
    double opt_minlo_kt_cut;
    double opt_minlo_corefactor;
    fastjet::JetDefinition clustering_def;
    double lambda;
    std::vector<double> minlo_scales;
    RescalerType opt_minlo_core;

    int opt_max_nparticle;  // used by LoopSim and MINLO for RS events
    int opt_born_alphaspower;  // used only for APPLgrid init
    int event_order;
    int coll_weights_count;
    double coll_weights[8];
    double pdf_f1[4];
    double pdf_f2[4];
    double naked_weight;
    double event_weight;
    double get_ren_scale() const { return orig_ren_scale()*ren_scalefactor; }
    double get_fac_scale() const { return orig_fac_scale()*fac_scalefactor; }
    double get_event_weight() const { return event_weight; }
    int get_event_order() const { return event_order; }

    // LoopSim parameters
    double opt_loopsim_R;
    int opt_loopsim_nborn;

    // qfilter
    int opt_filter_inq;
    int opt_filter_nq;

    // pdf sets
    int opt_frompdf, opt_topdf;
    int lhaid1, lhaid2;
    double x1r, x2r;
    int get_lhaid1() const { return lhaid1; }
    int get_lhaid2() const { return lhaid2; }
    double get_x1r() const { return x1r; }
    double get_x2r() const { return x2r; }

    double pdfx1[13];
    double pdfx2[13];
    double pdfx1p[13];
    double pdfx2p[13];

    // alphas
    bool use_sherpa_alphas;
    void initAS(const int order, const double asMZ, const double mZ2,
                const std::vector<double>& qmasses);
    SHERPA::One_Running_AlphaS* sherpa_alphas;

    // beta0 workaround
    int opt_beta0fix, opt_cdr2fdhfix, opt_pi2o12fix;
    static double beta0pole2(int id1_, int id2_, int n_, const Int_t* kf_);
    static double pole2(int id1_, int id2_, int n_, const Int_t* kf_);
    static double cdr2fdh(int id1_, int id2_, int n_, const Int_t* kf_);

    // Q2, x1, x2 stats
    void stat_report();
    double stat_Q2_min, stat_Q2_max;
    double stat_x1_min, stat_x1_max;
    double stat_x2_min, stat_x2_max;

    // counting events
    Int_t event_prev_id;
    long event_groups;
    double event_trials;
    bool new_event;

    // eventoscope
    int opt_stat_step;
    double xsval_cur, xserr_cur;
    std::vector<double> xsvals;
    std::vector<double> xserrs;

    // output peek
    int opt_histogram_step;

    // stats and warnings
    long print_event_step;
    long pdf_warning_limit;
    long pdf_warning_count;
    double pdf_warning_thresh;

  protected:
    double get_alphas(int pdf, double mur, bool use_sherpa=false);
    void prepare_event();
    void reweight(const PseudoJetVector& input,
                  const PseudoJetVector& jets);
    void reweight_pdfcheck();
    double reweight_applyfixes(double new_me_wgt, double log_r);
    void stat_update(double fac_scale, double ren_scale);

  public:
    intptr_t this_to_int() const { return reinterpret_cast<intptr_t>(this); }
};

#if defined(__MAKECINT__)
#pragma link C++ class fastjet::JetDefinition;
#endif

#endif
